
#### POR FAVOR, RESPONDA AS PERGUNTAS ABAIXO.

#### QUEREMOS SABER MAIS SOBRE VOCÊ. ESTA ETAPA É MUITO IMPORTANTE PARA ENTENDERMOS QUEM É VOCÊ. ESPERAMOS QUE VOCÊ DEDIQUE-SE ÀS RESPOSTAS, OS DETALHES SÃO MUITO IMPORTANTES.

1) Adicione aqui links para seu perfil profissional (Linkedin, por exemplo):

2) Descreva a sua equipe atual ou anterior. O que você gosta e não gosta? Se for seu primeiro emprego, nos conte o que considera uma equipe ideal.

3) Qual foi seu maior fracasso até o momento?

4) Como você descreve a sua personalidade?

5) Certo, e os seus colegas e o seu líder, como descreveriam a sua personalidade?

6) Cite as principais tecnologias (Front-end e Back-end) que já utilizou e como elas foram aplicadas no seu trabalho

7) Possui experiência com metodologias ágeis? Cite as principais atividades e práticas ágeis que você já aplicou nos projetos que participou

8) Cite as principais atividades que desempenhou nos 2 últimos projetos que participou e como elas contribuíram para o sucesso deles